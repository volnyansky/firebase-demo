package com.google.firebase.quickstart.auth;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 * Created by Stanislav Volnyansky on 23.08.16.
 */
@RunWith(AndroidJUnit4.class)
public class FacebookActivityTest {

    @Rule
    public ActivityTestRule<FacebookLoginActivity> mActivityTestRule =
            new ActivityTestRule<>(FacebookLoginActivity.class);
    private UiDevice uiDevice;

    @Test
    public void facebookLoginTest() {
        try {
            uiDevice = UiDevice.getInstance(getInstrumentation());
            uiDevice.findObject(By.res("com.google.firebase.quickstart.auth:id/button_facebook_login")).click();
            Thread.sleep(1000);
            String text=uiDevice.findObject(By.res("com.google.firebase.quickstart.auth:id/detail")).getText();
            Assert.assertTrue("FB id empty",!text.equals(""));

            uiDevice.findObject(By.res("com.google.firebase.quickstart.auth:id/button_facebook_signout")).click();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
