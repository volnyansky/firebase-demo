package com.google.firebase.quickstart.auth;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ChooserActivityTest {

    @Rule
    public ActivityTestRule<ChooserActivity> mActivityTestRule = new ActivityTestRule<>(ChooserActivity.class);

    @Test
    public void chooserActivityTest() {
        ViewInteraction twoLineListItem = onView(
                allOf(childAtPosition(
                        withId(R.id.list_view),
                        1),
                        isDisplayed()));
        twoLineListItem.perform(click());

        ViewInteraction loginButton = onView(
                allOf(withId(R.id.button_facebook_login), withText("Вход через Facebook"), isDisplayed()));
        loginButton.perform(click());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.button_facebook_signout), withText("Sign Out"), isDisplayed()));
        appCompatButton.perform(click());

        pressBack();

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
