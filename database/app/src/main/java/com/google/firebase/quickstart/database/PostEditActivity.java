package com.google.firebase.quickstart.database;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.quickstart.database.models.Post;

public class PostEditActivity extends AppCompatActivity {
    private static final String TAG = "PostDetailActivity";

    public static final String EXTRA_POST_KEY = "post_key";
    private DatabaseReference mDatabase;
    private EditText mTitleField;
    private EditText mBodyField;
    private String postKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        // [START initialize_database_ref]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END initialize_database_ref]

        mTitleField = (EditText) findViewById(R.id.field_title);
        mBodyField = (EditText) findViewById(R.id.field_body);
        postKey=getIntent().getStringExtra(EXTRA_POST_KEY);
        mDatabase.child("posts").child(getIntent().getStringExtra(EXTRA_POST_KEY)).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Post post=dataSnapshot.getValue(Post.class);
                        mTitleField.setText(post.title);
                        mBodyField.setText(post.body);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
        findViewById(R.id.fab_submit_post).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Post post=new Post();
                post.title=mTitleField.getText().toString();
                post.body=mBodyField.getText().toString();

                mDatabase.child("posts").child(postKey).updateChildren(post.toMap());


            }
        });

    }
}
