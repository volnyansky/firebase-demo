package com.google.firebase.quickstart.firebasestorage;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class SimpleService extends Service {
    public SimpleService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            Thread.sleep(20000);
            stopSelf();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }
}
