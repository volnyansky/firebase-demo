package com.google.firebase.quickstart.firebasestorage;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class MyIntentService extends IntentService {

    private static final int ONGOING_NOTIFICATION_ID =100 ;

    public MyIntentService() {
        super("IntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.common_ic_googleplayservices)
                .setProgress(100,0,false)
                .build();
        startForeground(ONGOING_NOTIFICATION_ID, notification);


        try {
            for(int i=0;i<100;i++){
                Thread.sleep(300);
                Intent intent1=new Intent("progress");
                intent1.putExtra("progress",i);
                LocalBroadcastManager.getInstance(getApplicationContext())
                        .sendBroadcast(intent1);
                notification = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.common_ic_googleplayservices)
                        .setProgress(100,i,false)
                        .build();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(ONGOING_NOTIFICATION_ID,notification);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
